package com.bsa.giphy.controller;

import com.bsa.giphy.config.Giphy;
import com.bsa.giphy.dto.GifQueryDto;
import com.bsa.giphy.service.GifsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.springframework.http.HttpStatus.NOT_FOUND;

@RestController
@RequestMapping("/user")
public class GifsController {
    @Autowired
    GifsService gifsService;
    @Autowired
    Giphy giphy;

    @PostMapping("/{id}/generate")
    public void generateGif(@PathVariable String id, @RequestBody GifQueryDto query) {
        if (!gifsService.isGifExistsInCache(query.getQuery())) {
            try {
                gifsService.loadGif(query);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        var gifPath = gifsService.findGifInCache(query.getQuery());
        var userGifFolder = Paths.get(gifPath.getParent().toString().replaceFirst("cache", "users/" + id));
        try {
            if (!Files.exists(userGifFolder.getParent())) {
                Files.createDirectories(userGifFolder.getParent());
                Files.createFile(Paths.get(userGifFolder.getParent() + "/history.csv"));
            }

            if (!Files.exists(userGifFolder)) {
                Files.createDirectories(userGifFolder);
            }

            var userGifPath = Paths.get(userGifFolder + "/" + gifPath.getFileName());
            if (!Files.exists(userGifPath)) {
                gifsService.copyGifToUsersFolder(gifPath, userGifPath);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @GetMapping("{id}/{gif}")
    public String getGif(@PathVariable String id, @PathVariable String gif) {
        if (gifsService.isGifExistsInCache(gif)) {
            return gifsService.findGifInCache(gif).toAbsolutePath().toString();
        }

        var userGifPath = gifsService.findUserGif(id, gif);
        if (userGifPath != null)
            return userGifPath.toAbsolutePath().toString();

        throw new ResponseStatusException(NOT_FOUND, "Unable to find gif");
    }
}
