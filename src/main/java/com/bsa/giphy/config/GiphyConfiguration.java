package com.bsa.giphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GiphyConfiguration {
    @Bean
    public Giphy giphy(GiphyConfigurationProperties configurationProperties) {
        return new Giphy(configurationProperties.getApiKey());
    }
}
