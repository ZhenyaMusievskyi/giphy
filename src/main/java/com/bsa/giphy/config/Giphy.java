package com.bsa.giphy.config;

import lombok.Getter;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class Giphy {
    private String apiKey;
    private String urlTemplate;
    private String gifUrlTemplate;

    public Giphy(String apiKey) {
        this.apiKey = apiKey;
        urlTemplate = String.format("http://api.giphy.com/v1/gifs/search?api_key=%s&limit=3", apiKey) + "&q=%s";
        gifUrlTemplate = "https://i.giphy.com/media/%s/giphy.gif";
    }

    public String getURL(String param) {
        return String.format(urlTemplate, param);
    }

    public String getGifUrl(String gifId) {
        if (gifId != null) {
            return String.format(gifUrlTemplate, gifId);
        }
        return null;
    }
}
