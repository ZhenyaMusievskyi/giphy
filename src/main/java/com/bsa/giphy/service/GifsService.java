package com.bsa.giphy.service;

import com.bsa.giphy.config.Giphy;
import com.bsa.giphy.dto.GifQueryDto;
import com.bsa.giphy.utils.JsonMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.*;
import java.net.URI;
import java.net.URL;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@Service
public class GifsService {
    @Autowired
    Giphy giphy;
    @Autowired
    JsonMapper mapper;

    private File cacheDirectory;
    private File usersDirectory;

    public GifsService() {
        cacheDirectory = new File("cache/");
        if (!cacheDirectory.exists()) {
            cacheDirectory.mkdir();
        }

        usersDirectory = new File("users/");
        if (!usersDirectory.exists()) {
            usersDirectory.mkdir();
        }
    }

    public boolean isGifExistsInCache(String gifFolder) {
        var path = Paths.get(cacheDirectory.getPath() + "/" + gifFolder);
        return Files.exists(path);
    }

    public Path findGifInCache(String gifFolder) {
        var path = Paths.get(cacheDirectory.getPath() + "/" + gifFolder);
        try {
            return Files
                    .list(path)
                    .findFirst()
                    .orElse(null);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public Path findUserGif(String user, String gifFolder) {
        var path = Paths.get(usersDirectory.getPath() + "/" + user + "gifFolder");
        try {
            if (Files.exists(path)) {
                return Files
                        .list(path)
                        .findFirst()
                        .orElse(null);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    public void loadGif(GifQueryDto queryDto) throws IOException, InterruptedException {
        var client = HttpClient.newHttpClient();
        var request = HttpRequest.newBuilder()
                .uri(URI.create(giphy.getURL(queryDto.getQuery())))
                .build();

        var response = client.send(request, HttpResponse.BodyHandlers.ofString());
        mapper.getGifIdFromJson(response.body(), queryDto);

        var gif = getGifBytes(giphy.getGifUrl(queryDto.getGifId()));
        File pathToLoad = new File(cacheDirectory.getPath() + "/" + queryDto.getQuery());
        File loadedGif = new File(pathToLoad.getPath() + "/" + queryDto.getGifId() + ".gif");
        pathToLoad.mkdirs();
        loadedGif.createNewFile();
        writeGif(loadedGif, gif);
    }

    public void copyGifToUsersFolder(Path gifPath, Path destination) {
        try {
            Files.copy(gifPath, destination);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private byte[] getGifBytes(String url) {
        try (InputStream stream = new URL(url).openConnection().getInputStream()) {
            return stream.readAllBytes();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private void writeGif(File file, byte[] gif) {
        try (BufferedOutputStream outputStream = new BufferedOutputStream(new FileOutputStream(file))) {
            outputStream.write(gif);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
