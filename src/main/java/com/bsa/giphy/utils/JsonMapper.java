package com.bsa.giphy.utils;

import com.bsa.giphy.dto.GifQueryDto;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

@Service
public class JsonMapper {
    public GifQueryDto getGifIdFromJson(String json, GifQueryDto gifQueryDto) {
        JSONObject jsonObject = new JSONObject(json);
        JSONArray dataArray = jsonObject.getJSONArray("data");
        if (dataArray.length() >= 0) {
            jsonObject = (JSONObject) jsonObject.getJSONArray("data").get(0);
            gifQueryDto.setGifId(jsonObject.getString("id"));
        }

        return gifQueryDto;
    }
}
