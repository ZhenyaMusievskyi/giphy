package com.bsa.giphy.dto;

import lombok.Data;
import lombok.Getter;

@Data
public class GifQueryDto {
    String user;
    String query;
    String gifId;
}
